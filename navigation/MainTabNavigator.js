import React from 'react'
import { Platform } from 'react-native'
import { createStackNavigator, createBottomTabNavigator, createMaterialTopTabNavigator } from 'react-navigation'
import TabBarIcon from '../components/TabBarIcon'

import MenuScreen from '../screens/MenuScreen'
import TicketScreen from '../screens/TicketScreen'
import ProfileScreen from '../screens/ProfileScreen'
import Colors from '../constants/Colors'

const MenuStack = createStackNavigator({
  Menu: MenuScreen,
})

const TicketStack = createStackNavigator({
  Ticket: TicketScreen,
})

const ProfileStack = createStackNavigator({
  Profile: ProfileScreen,
})

MenuStack.navigationOptions = {
  tabBarLabel: 'Ruokalista',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-restaurant${focused ? '' : '-outline'}`
          : 'md-restaurant'
      }
    />
  ),
}

TicketStack.navigationOptions = {
  tabBarLabel: 'Tickets',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-cash' : 'md-cash'}
    />
  ),
}

ProfileStack.navigationOptions = {
  tabBarLabel: 'Profile',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-person' : 'md-person'}
    />
  ),
}

//
// createBottomTabNavigator (the newer one, no transition/animation)
//
// export default createBottomTabNavigator({
//   HomeStack,
//   OtherStack,
// })

//
// createMaterialTopTabNavigator (old one, material design with animation)
//
export default createMaterialTopTabNavigator({
  MenuStack,
  TicketStack,
  ProfileStack
},
{
  tabBarOptions: {
    upperCaseLabel: false,
    activeTintColor: Colors.laureaBlue,
    pressColor: '#76CEE8',
    inactiveTintColor: Colors.inactiveColor,
    showIcon: true,
    labelStyle: {
      fontSize: 12
    },
    style: {
      backgroundColor: Colors.headerColor,
      shadowColor: Colors.laureaBlue,
      shadowOffset: {
        width: 0,
        height: 0,
      },
      shadowOpacity: 0,
      shadowRadius: 0,
      elevation: 12,
      height: 57,
      borderBottomWidth: 0,
      borderBottomColor: '#fff'
    },
    indicatorStyle: {
      height: 2,
      backgroundColor: Colors.laureaBlue
    },
    labelStyle: {
      margin: 0,
      padding: 0
    }
  },
  tabBarPosition: 'bottom'
})
