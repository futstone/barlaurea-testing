//const tintColor = '#2f95dc'
const tintColor = '#00a0d1'

export default {
  tintColor,
  tabIconDefault: '#7a7a7a',
  inactiveColor: '#7a7a7a',
  tabIconSelected: tintColor,
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
  textColor: '#bcbcbc',
  textColorDark: '#757575',
  buttonColor: '#f77fb3',
  headerColor: '#fff',
  laureaBlue: '#00a0d1',
  laureaPink: '#f77fb3',
  //lightgray: '#383838',
  lightgray: '#fff',
  //lightgray: '#331f09',
  //darkgray: '#303030',
  darkgray: '#fff',
  //darkgray: '#2d1c08',
}

// darkgray: 303030
// lightgray: 383838
// laureablue: 00a0d1
// laureapink: f77fb3