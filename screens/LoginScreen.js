import React from 'react'
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Button
} from 'react-native'

import { styles } from '../styles/styles'
import Colors from '../constants/Colors'

export default class LoginScreen extends React.Component {
  static navigationOptions = {
    title: 'LoginScreen!',
  }

  login = () => {
    this.props.navigation.navigate('Main')
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>This is the LoginScreen.js</Text>
        <Button title="Login" onPress={this.login} color={Colors.buttonColor} />
      </View>
    )
  }
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// })