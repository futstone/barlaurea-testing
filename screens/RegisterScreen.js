import React from 'react'
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Button
} from 'react-native'

import { styles } from '../styles/styles'
import Colors from '../constants/Colors'

export default class RegisterScreen extends React.Component {
  static navigationOptions = {
    title: 'RegisterScreen!',
  }

  register = () => {
    this.props.navigation.navigate('Main')
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>This is the RegisterScreen.js</Text>
        <Button title="Register" onPress={this.register} color={Colors.buttonColor} />
      </View>
    )
  }
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// })