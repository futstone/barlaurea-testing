import React from 'react'
import {
  FlatList,
  Image,
  Modal,
  ScrollView,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
  Button
} from 'react-native'

import QRCode from 'react-native-qrcode'
import { styles } from '../styles/styles'
import Colors from '../constants/Colors'

export default class TicketScreen extends React.Component {
  state = {
    tickets: [
      { ticketId: '635478765343', prodName: 'Lunch' },
      { ticketId: '712534512376', prodName: 'Lunch' }
    ],
    modalVisible: false
  }

  static navigationOptions = {
    title: 'TicketScreen',
    headerStyle: styles.headerStyle,
    headerTitleStyle: styles.headerTitleStyle
  }

  onPressTicketItem = (item) => {
    console.log("pressed item " + item.prodName)
    this.setState({ modalVisible: true })
  }

  setModalVisible = (visible) => {
    this.setState({ modalVisible: visible })
  }

  render() {
    return (
      <View style={styles.container}>
        <Modal
          animationType='fade'
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => console.log("Modal closed.")}>
        
          <View style={styles.modal}>
            <Text>Modal..</Text>
            
            <QRCode
              value={this.state.tickets[0].ticketId}
              size={300}
              bgColor='#000000'
              fgColor={Colors.laureaPink}
            />
            
            <Button
              title='Close' 
              onPress={() => this.setModalVisible(!this.state.modalVisible)} 
            />
          </View>
        </Modal>

        <FlatList
          data={ this.state.tickets }
          keyExtractor={(item) => item.ticketId}
          renderItem={(item) => {
            return (
              <TouchableHighlight
                onPress={ () => this.onPressTicketItem(item.item) }
                underlayColor={Colors.laureaPink}>

                <View style={styles.ticketListItem}>
                  <Text style={styles.ticketListItemText}>{ item.item.prodName }</Text>
                </View>
              </TouchableHighlight>
            )
          }}
        />
        <Text>This is the TicketScreen.js</Text>
      </View>
    )
  }
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// })
