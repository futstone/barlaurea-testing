import React from 'react'
import { View, Text } from 'react-native'
import { Icon } from 'expo'

import { styles } from '../styles/styles'
import Colors from '../constants/Colors'

export default class DayHeader extends React.Component {
  render() {
    return (
      <View style={styles.dateHeaderContainer}>
        <Text style={styles.dateHeaderText}>{this.props.item.dateString}</Text>
        <Icon.Ionicons
          name={'md-arrow-down'}
          size={20}
          //style={{ marginBottom: -6, marginTop: -6 }}
          color={ Colors.laureaPink }
        />
      </View>
    )
  }
}