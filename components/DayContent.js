import React from 'react'
import { View, Text } from 'react-native'

import { styles } from '../styles/styles'
import Colors from '../constants/Colors'

export default class DayContent extends React.Component {
  render() {
    const item = this.props.item
    return (
      <View style={styles.dateContainer}>
      { item.mealoptions.map(meal => {
          return (
            <View style={styles.mealContainer} key={item.date+"-"+meal.id}>
              <Text style={{fontWeight: 'bold', color: Colors.textColorDark}}>{meal.name}</Text>
              { meal.menuItems.map(food => <Text key={food.name} style={{color: Colors.textColorDark}}>{food.name}</Text>) }
            </View>
          )
        })}
      </View>
    )
  }
}