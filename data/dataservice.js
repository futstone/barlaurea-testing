import { Divider } from "react-native-paper";

const url = "http://www.jamix.fi/ruokalistapalvelu/rest/haku/menu/97090/1?lang=fi"
const weekDays = [
  "Sunnuntai",
  "Maanantai",
  "Tiistai",
  "Keskiviikko",
  "Torstai",
  "Perjantai",
  "Lauantai",
]

const findVal = (object, key) => {
  var value
  Object.keys(object).some(function(k) {
    if (k === key) {
      value = object[k]
      return true
    }
    if (object[k] && typeof object[k] === 'object') {
      value = findVal(object[k], key)
      return value !== undefined
    }
  })
  return value
}

const getDateString = (date, daynr) => {
  const dd = date.slice(6,8)
  const mm = date.slice(4,6)
  const dayName = weekDays[daynr]
  return `${dayName} (${dd}-${mm})`
}

const formatData = (data) => {
  data = data.sort((a, b) => b.date < a.date)
  let resModified = []
  
  data.forEach(day => {
    resModified = resModified.concat(
      {
        dateString: getDateString(day.date.toString(), day.weekday),
        header: true
      },
      {
        ...day
      }
    )
  })
  return resModified
}

export const fetchMenu = async () => {
  let res = await fetch(url).then(res => res.json())
  //res = await res.json()
  const days = findVal(res, 'days')
  const resultFormatted = formatData(days)
  return resultFormatted
}

// resultFormatted data structure:
// [
//   {
//     dateString: "...",
//     mealoptions: [
//       {
//         name: "Lounas",
//         menuItems: [
//           {
//             name: "food1",
//             ingredients: "..."
//           },
//           {
//             name: "food2",
//             ingredients: "..."
//           }
//         ]
//       },
//       {
//         name: "Keittolounas",
//         menuItems: [
//           {
//             name: "food1",
//             ingredients: "..."
//           },
//           {
//             name: "food2",
//             ingredients: "..."
//           }
//         ]
//       },
//       {
//         name: "Kasvislounas",
//         menuItems: [
//           {
//             name: "food1",
//             ingredients: "..."
//           },
//           {
//             name: "food2",
//             ingredients: "..."
//           }
//         ]
//       }
//     ]
//   }
// ]